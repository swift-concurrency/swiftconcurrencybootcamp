//
//  TaskBootcamp.swift
//  SwiftConcurrencyBootcamp
//
//  Created by Sergio Andres Rodriguez Castillo on 22/01/24.
//

import SwiftUI

class TaskBootcampViewModel: ObservableObject {
    @Published var image: UIImage? = nil
    @Published var image2: UIImage? = nil
    
    func fetchImage() async {
        do {
            guard let url = URL(string: "https://picsum.photos/200") else { return }
            let (data, response) = try await URLSession.shared.data(from: url, delegate: nil)
            
            self.image = UIImage(data: data)
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func fetchImage2() async {
        do {
            guard let url = URL(string: "https://picsum.photos/200") else { return }
            let (data, response) = try await URLSession.shared.data(from: url, delegate: nil)
            
            self.image2 = UIImage(data: data)
        } catch {
            print(error.localizedDescription)
        }
    }
}

struct TaskBootcamp: View {
    @StateObject private var viewModel = TaskBootcampViewModel()
    
    var body: some View {
        VStack(spacing: 40) {
            if let image = viewModel.image {
                Image(uiImage: image)
                    .resizable()
                    .scaledToFit()
                    .frame(width: 200, height: 200)
            }
            if let image = viewModel.image2 {
                Image(uiImage: image)
                    .resizable()
                    .scaledToFit()
                    .frame(width: 200, height: 200)
            }
        }
        .onAppear {
            //            Task {
            //                await viewModel.fetchImage()
            //            }
            //            Task {
            //                await viewModel.fetchImage2()
            //            }
            
            Task(priority: .low) {
                print("LOW: \(Thread.current) : \(Task.currentPriority.rawValue)")
            }
            Task(priority: .medium) {
                print("MEDIUM: \(Thread.current) : \(Task.currentPriority.rawValue)")
            }
            Task(priority: .high) {
                print("HIGH: \(Thread.current) : \(Task.currentPriority.rawValue)")
            }
            Task(priority: .background) {
                print("BACKGROUND: \(Thread.current) : \(Task.currentPriority.rawValue)")
            }
            Task(priority: .utility) {
                print("UTILITY: \(Thread.current) : \(Task.currentPriority.rawValue)")
            }
            Task(priority: .userInitiated) {
                print("USERINITIATED: \(Thread.current) : \(Task.currentPriority.rawValue)")
            }
        }
    }
}

#Preview {
    TaskBootcamp()
}
