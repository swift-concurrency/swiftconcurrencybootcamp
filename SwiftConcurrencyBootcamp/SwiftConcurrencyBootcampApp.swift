//
//  SwiftConcurrencyBootcampApp.swift
//  SwiftConcurrencyBootcamp
//
//  Created by Sergio Andres Rodriguez Castillo on 18/01/24.
//

import SwiftUI

@main
struct SwiftConcurrencyBootcampApp: App {
    var body: some Scene {
        WindowGroup {
            //ContentView()
            //DownloadImageAsync()
            //AsyncAwaitBootcamp()
            //TaskBootcamp()
            AsyncLetBootcamp()
        }
    }
}
